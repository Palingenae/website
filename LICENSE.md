# Droits d'utilisation
## TL;DR

Ce site web est sous licence **Copyleft CC-BY-SA**. Copiez, modifiez, partagez et n'ajoutez pas plus de contrainte.

## Le code

Bien qu'il serve pour une présence personnelle sur Internet, vous avez le droit d'utiliser le code source de ce site web afin de :

* **le copier**,
    * cloner le dépôt,
* **le modifier**,
    * apporter des corrections,
    * vous le réapproprier,
* **le partager**,
    * via mon domaine pour m'aider à me faire connaître,
    * montrer vos changements.

Selon la licence _Creative Commons Attribution and Share Alike_, vous devez attribuer le nom de l'auteur de ce site web si vous le copiez et le modifiez. Étant donné qu'il est disponible sur Github, vous pouvez le forker pour le modifier pour votre utilisation, sinon pour corriger les erreurs qui peuvent subsister. Si vous le clonez, il est demandé de mettre dans votre README.md le lien du dépôt d'origine. Si avez oublié, ne touchez pas au commentaire présent dans le code de la page.

Tout nouveau projet qui utilise comme base le code de ce site web devra continuer à exister sous CC-BY-SA, et ne pourra pas être affublé d'une licence plus contraignante que celle que je décerne.

## Les écrits

Le contenu (à savoir les publications écrites de ce site web) héritent de la licence sous laquelle ce site est régit. Si vous reprennez une partie de mes écrits dans une citation, vous vous devez d'appliquer un lien vers la page d'origine de celle-ci. C'est prévu dans la licence.

Il existe de la part de certains journalistes une pratique qui consiste à, justement, ne pas attribuer l'auteur d'une citation, alors qu'il est déjà requis dans les études de journalisme de référencer le producteur de l'écrit. Si vous n'avez pas fait d'études de journalisme, ce n'est pas grave : il vous suffit tout simplement de poser un lien, mon nom et/ou pseudo, et la licence. C'est certes un peu lourd, mais ce serait vraiment sympa de procéder ainsi.

Vous avez le droit de modifier mes écrits. D'un certain point de vue, ceci est vrai, parce que je peux néanmoins continuer à créer des fautes d'orthographe, de grammaire, de style et de syntaxe. Si en dehors des corrections, vous venez à tourner mes phrases dans le sens de me faire dire ce que je n'ai pas dit (calomnies et diffamations), cela figure comme un délit au regard de la loi belge, à minima.

Je n'ai pas appliqué de licence contraignante, il serait vraiment malvenu d'en abuser.

## Les photos

Vous pouvez utiliser mes photos pour illustrer vos articles, les modifier pour leur donner un style pour vous et les republier. Si vous les modifiez, vous ne pouvez pas enlever l'attribution présente sur la photo (watermark) et dans les métadonnées. Je vous rassure, la watermark est très discrète et n'impacte pas la photo.

Si vous voulez publier mes photos sur vos sites web, faites comme bon vous semble. Par contre, vous ne pouvez pas publier mes photos sur Facebook et Instagram. Je ne le fais pas moi-même à cause de leur politique d'utilisation.

### Source (en anglais)
["**Human readable**"](https://creativecommons.org/licenses/by-sa/4.0/) - [**Licence complète**](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
